Static content hosted on GitLab Pages for various purposes.

All content will be automatically published on merge.
https://rpfinneran.gitlab.io/finn-static-sites/

Example: https://rpfinneran.gitlab.io/finn-static-sites/8459-carmela.zip 
